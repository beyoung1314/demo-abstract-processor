### 辅助文档
[AbstractProcessor 注解拦截器的使用](https://www.bilibili.com/read/cv14287978)
### 项目结构
```
├── demo-app             -- 业务项目
├── demo-auto            -- AbstractProcessor注解处理器项目
├── demo-component       -- 公共项目，需要使用注解拦截器的项目
```