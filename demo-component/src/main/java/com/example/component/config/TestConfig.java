package com.example.component.config;

import com.example.component.entity.BlyEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.auto.processor.BeanAuto;

/**
 * @Description TODO
 * @Author Benjamin
 * @Date 2021/12/4
 */
@Configuration
@BeanAuto
public class TestConfig {

	@Bean
	public BlyEntity blyEntity(){
		BlyEntity blyEntity = new BlyEntity();
		blyEntity.setName("我是槟榔瘾");
		return blyEntity;
	}

}
